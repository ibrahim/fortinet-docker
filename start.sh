#!/bin/sh

# change defult vpn site here
VPN_CONFIG="vpnsite-site1.conf"
if [ -n "$VPNSITE" ] ; then
   VPN_CONFIG="vpnsite-${VPNSITE}.conf"
fi

export VPNTIMEOUT=${VPNTIMEOUT:-5}

# Setup masquerade, to allow using the container as a gateway
for iface in $(ip a | grep eth | grep inet | awk '{print $2}'); do
  iptables -t nat -A POSTROUTING -s "$iface" -j MASQUERADE
done

openfortivpn -c "/vpn/$VPN_CONFIG"

