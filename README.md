# fortinet-docker

run fortinet vpn on docker using [openfortivpn](https://github.com/adrienverge/openfortivpn)

## Requirements

* Linux
* Docker
* GNU Make

## Overview

- Run Fortinet VPN inside a Docker container
- Access the VPN network from the Docker Host using a Docker network (using NAT)
- This is useful if Fortinet VPN blocks Internet access (split tunneling is disabled) once connected to VPN

## Installation

* Clone the repo
```
$ git clone https://gitlab.com/ibrahim/fortinet-docker.git 
```

* Create config files for VPN sites using `vpnsite-template.conf` as template
```
$ cd fortinet-docker
$ cp vpnsite-template.conf vpnsite-site1.conf
$ cp vpnsite-template.conf vpnsite-site2.conf
$ vi vpnsite-site1.conf
$ vi vpnsite-site2.conf
```

* Edit `start.sh` and point `VPN_CONFIG` to your default VPN site config
```
$ vi start.sh
```

* Create Docker Network to access the VPN network from Docker host
```
$ docker network create --driver bridge --subnet=172.20.0.0/24 fortinet_network
```

* Create route to VPN on Docker host via Docker network / container
```
$ sudo ip route add x.x.x.x/y via 172.20.0.1
```

* Build Docker Image
```
$ make build
```

* Start VPN to default site 
```
$ make run
```

* Start VPN to `site2` (non-default) by passing `VPNSITE`. This will use `vpnsite-site2.conf` config file
```
$ make run VPNSITE=site2
```

## Remote Access

Once the VPN is running within Docker container, you can access the remote servers.

* Access VPN nodes from Docker Host
```
$ ssh user@server
```

* You can open a bash a shell to the Docker container and connect to the remote servers from within the docker container. Replace `<id>` with the Docker container id
```
$ docker ps
$ docker exec -it <id> /bin/bash

docker$ ssh user@server
```


## Copyright and License

Copyright (c) 2020 Mohamed Ibrahim. Code released under the [MIT](LICENSE) license.

