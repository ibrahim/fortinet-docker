DOCKER_NETWORK=fortinet_network
DOCKER_SUBNET=172.20.0.0/24
DOCKER_IP=172.20.0.2
VPNSITE=site1

fortinetnetwork:=$(shell docker network ls | grep $(DOCKER_NETWORK))

build:
	@echo building docker image
	docker build -t ibrahim/fortinet .

network:
	@echo creating docker network
 	ifeq (,$(findstring $(DOCKER_NETWORK),$(fortinetnetwork)))
	docker network create --driver bridge --subnet=$(DOCKER_SUBNET) $(DOCKER_NETWORK)
 	else
	@echo docker network already exists, skipping docker network creation	
 	endif

addroute:
	@echo Make sure you have routes added to the VPN network from the host machine
	@echo eg. sudo ip route add 10.0.0.0/8 via ${DOCKER_IP}
	@echo 

run: network addroute
	@echo starting vpn for site ${VPNSITE}
	@echo to use another vpn site, pass VPNSITE parameter to make
	@echo eg. make run VPNSITE=site2
	@echo
	docker run -it --rm \
	--env VPNSITE=${VPNSITE} \
	--privileged \
	--net ${DOCKER_NETWORK} \
	--ip ${DOCKER_IP} \
	ibrahim/fortinet



