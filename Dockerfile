FROM ubuntu:18.04

RUN apt-get update && \
  apt-get install -y expect wget iputils-ping net-tools iproute2 ppp ssh curl iptables openfortivpn && \
  rm -rf /var/lib/apt/lists/* && \
  /sbin/sysctl -w net.ipv4.conf.all.forwarding=1

WORKDIR /vpn


# Copy runfiles
COPY vpnsite-*.conf /vpn/
COPY start.sh /vpn/start.sh

CMD [ "/vpn/start.sh" ]

